import {LitElement} from './node_modules/lit-html'

class MyApp extends LitElement{
	constructor() {
		super();
	}

	static get properties() {
		return {score: '3'}
	}

	_render() {
		console.log('the score is: ', score);
		return html`The score is ${score}`
	}

	static get is() {
		return `my-app`
	}
}

window.customElements.define(MyApp.is, MyApp)


